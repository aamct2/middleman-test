BUNDLER := $(shell which bundler)
GEM := $(shell which gem)
NPM := $(shell which npm)
RUBY := $(shell which ruby)

# - Build & Set Up

# Builds the blog
build:
	$(BUNDLER) exec middleman build
.PHONY: build

# Sets up the workspace (typically used after a fresh clone)
# - Installs dependencies, builds the site
setup: dependencies
	$(MAKE) build
.PHONY: setup

# - Lint

# Lints the source files
lint:
	$(BUNDLER) exec rubocop
.PHONY: lint

# - Dependencies

# Installs the dependencies
dependencies:
ifndef RUBY
	$(error "Couldn't find Ruby installed.")
endif
	@$(MAKE) install-bundler install-node

# Installs bundler (or installs the gems if already installed)
install-bundler:
ifndef BUNDLER
	$(GEM) install bundler
	bundle install --path vendor
else
	$(BUNDLER) install --path vendor
endif

install-node:
ifndef NPM
	$(error "Couldn't find Node installed.")
endif
	$(NPM) install
