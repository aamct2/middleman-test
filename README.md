### Middleman Test

This is a test repository for using the [Middleman][middleman] software for generating a static site.

### Thanks

Thank you to [Ash Furrow's blog][furrow] as an example.

### License

This repository is licensed under the [MIT license][license].

<!-- Links -->

[furrow]: https://github.com/ashfurrow/blog
[license]: ./LICENSE
[middleman]: https://middlemanapp.com/
