
# Build-specific configuration
configure :build do
  set :build_dir, 'public'
  set :base_url, "/middleman" # baseurl for GitLab Pages (project name) - leave empty if you're building a user/group website
  activate :relative_assets # Use relative URLs
end

###
# Blog settings
###

set :markdown_engine, :redcarpet
set :markdown, fenced_code_blocks: true, disable_indented_code_blocks: true,
               strikethrough: true, smartypants: true, with_toc_data: true
set :relative_links, true

activate :blog do |blog|
  # This will add a prefix to all links, template references and source paths
  blog.prefix = 'blog'
  blog.permalink = '{title}.html'

  blog.summary_separator = /\(READMORE\)/
  blog.new_article_template = File.expand_path('new_article.markdown.erb',
                                               File.dirname(__FILE__))
  blog.layout = 'partials/_blog_post'
end

###
# Other settings
###

activate :syntax

page '/*.xml', layout: false
